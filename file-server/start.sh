#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

docker run --rm -d --name my-nginx -v "$(pwd)/files:/usr/share/nginx/html:ro" -d -p $TEST_PORT:80 nginx